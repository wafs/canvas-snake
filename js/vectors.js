function Vector2d(x, y) {
    this.x = x || 0;
    this.y = y || 0;

    return this;
}

// "Static" JS functions
Vector2d.addVectors = function (vector1, vector2) {
    return new Vector2d(vector1.x + vector2.x, vector1.y + vector2.y);
};

Vector2d.subtractVectors = function (vector1, vector2) {
    return new Vector2d(vector1.x - vector2.x, vector1.y - vector2.y);
};

Vector2d.multiplyVectors = function (vector1, vector2) {
    return new Vector2d(vector1.x * vector2.x, vector1.y * vector2.y);
};

Vector2d.divideVectors = function (vector1, vector2) {
    return new Vector2d(vector1.x / vector2.x, vector1.y / vector2.y);
};

Vector2d.divideScalar = function(vector, scalar){
  return new Vector2d(vector.x / parseFloat(scalar), vector.y / parseFloat(scalar));
};

Vector2d.multiplyScalar= function(vector, scalar){
    return new Vector2d(vector.x * scalar, vector.y * scalar);
};

// Normal JS functions
Vector2d.prototype.add = function (vector) {
    this.x += vector.x;
    this.y += vector.y;

    return this;
};

Vector2d.prototype.subtract = function (vector) {
    this.x -= vector.x;
    this.y -= vector.y;

    return this;
};

Vector2d.prototype.multiply = function (scalar) {
    this.x *= scalar;
    this.y *= scalar;

    return this;
};

Vector2d.prototype.divide = function (scalar) {
    this.x /= scalar;
    this.y /= scalar;

    return this;
};


Vector2d.prototype.magnitude = function () {
    return Math.sqrt((this.x * this.x) + (this.y * this.y));
};


Vector2d.prototype.normalize = function () {
    var m = this.magnitude();
    if (m != 0) {
        this.divide(m);
    }

    return this;
};

Vector2d.prototype.limit = function (max) {
    if (this.magnitude() > max) {
        this.normalize().multiply(max);
    }
};

