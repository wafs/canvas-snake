
/**
 * Graph Data Structure.
 * @returns {Graph}
 * @constructor
 */

function Graph() {
    this.nodes = [];
    this.edges = [];


    this.Node = function (label) {
        this.label = label || '';
        this.value = 0;
        this.neighbours = [];

        return this;
    };


    this.Edge = function () {
        this.cost = 0;
        this.direction = 0;
        this.start = undefined;
        this.end = undefined;
    };
    return this;
}


Graph.prototype.addNode = function (node) {
    if (typeof node === "string") {
        node = new this.Node(node);
    }

    for (n in this.nodes) {

        if (this.nodes[n] === node) {
            return this;
        }
    }


    this.nodes.push(node);

    return this;
};

Graph.prototype.findNode = function (label) {
    for (var n in this.nodes) {
        if (this.nodes[n].label === label) {
            return this.nodes[n];
        }
    }

    return false;

};

Graph.prototype.connect = function (a, b, cost) {
    if (typeof a === "string") {
        a = this.findNode(a);
    }
    if (typeof b === "string") {
        b = this.findNode(b);
    }

    if (!(a && b)) {
        throw new Error("Could not find requested nodes to make an edge.");
    }

    var e = new this.Edge();
    e.start = a;
    e.end = b;
    e.cost = cost;
    this.edges.push(e);

    if (!this.isNeighbour(a, b)) {
        a.neighbours.push(b);
        b.neighbours.push(a);
    }

    return this;

};

Graph.prototype.isNeighbour = function (a, b) {
    return _.includes(a.neighbours, b);
};


/**
 * Disjoint Set aka Union Find
 * @returns {DisjointSet}
 * @constructor
 */
function DisjointSet() {
    this.sets = [];
    return this;
}

DisjointSet.prototype.makeSet = function (element) {
    this.sets.push([element]);
    return this;
};

DisjointSet.prototype.findSet = function (element) {
    for (var s in this.sets) {
        if (this.sets[s].indexOf(element) > -1) {
            return this.sets[s];
        }
    }

    return undefined;
};

DisjointSet.prototype.mergeSet = function (set1, set2) {
    _.pull(this.sets, set1, set2);
    var u = _.union(set1, set2);
    this.sets.push(u);
    return this;
};


/////////////////
// Bombs Away! //
/////////////////

function KruskalMST(graph) {

    var sortedEdges = _.sortBy(graph.edges, function (n) {
        return n.cost;
    });

    var g = new Graph();
    var d = new DisjointSet();

    for (var v in graph.nodes) {          // n
        graph.nodes[v].neighbours = [];
        d.makeSet(graph.nodes[v]);
    }

    for (var edge in sortedEdges) {
        var e = sortedEdges[edge];
        var startSet = d.findSet(e.start);
        var endSet = d.findSet(e.end);
        if (startSet != endSet) {
            g.addNode(e.start).addNode(e.end).connect(e.start, e.end, e.cost);
            d.mergeSet(startSet, endSet);
        }
    }
    return g;
}