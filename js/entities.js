function Entity() {
    this.velocity = new Vector2d();
    this.acceleration = new Vector2d();
    this.position = new Vector2d();
    this.position['top'] = this.position['bottom'] = this.position['left'] = this.position['right'] = 0;
    this.position['center'] = new Vector2d();
    this.dimensions = {width: 1, height: 1};
    this.fillStyle = '#AAAAAA';
    this.lineWidth = 2;
    this.strokeStyle = '#333333';
    this.updateCallback = undefined;
    this.updatePreCall = undefined;
    this.mass = 1;
    this.ignoreForces = false;
    this.internalTimer = 0;

    this.update = function () {
        if (typeof this.updatePreCall === 'function') {
            this.updatePreCall();
        }
        this.internalTimer++;

        this.velocity.add(this.acceleration);
        this.velocity.limit(10);
        this.position.add(this.velocity);
        this.position.center.x = this.position.x + this.dimensions.width;
        this.position.center.y = this.position.y - this.dimensions.height;

        if (typeof this.updateCallback === 'function') {
            this.updateCallback();
        }
        this.acceleration.multiply(0);
    };

    this.applyForce = function (force) {
        if (this.ignoreForces) {
            return;
        }
        var f = Vector2d.divideScalar(force, this.mass);
        this.acceleration.add(f);
    };


}

var DrawFactory = function (ctx) {

    var ball = function () {
        ctx.save();
        ctx.moveTo(this.position.x, this.position.y);
        ctx.beginPath();
        ctx.arc(this.position.center.x,
            this.position.center.y,
            this.dimensions.width,
            0, 2 * Math.PI, false);
        ctx.fillStyle = this.fillStyle;
        ctx.lineWidth = this.lineWidth;
        ctx.strokeStyle = this.strokeStyle;

        ctx.fill();
        ctx.stroke();
        ctx.restore();

    };

    var square = function () {
        ctx.save();
        ctx.moveTo(this.position.x, this.position.y);
        ctx.beginPath();
        ctx.fillStyle = this.fillStyle;
        ctx.lineWidth = this.lineWidth;
        ctx.strokeStyle = this.strokeStyle;
        ctx.fillRect(this.position.center.x - (this.dimensions.width / 2), this.position.center.y - (this.dimensions.width / 2),
            this.dimensions.width, this.dimensions.height);
        //ctx.strokeRect(this.position.center.x - (this.dimensions.width/2), this.position.center.y - (this.dimensions.width/2),
        //    this.dimensions.width, this.dimensions.height);
        ctx.closePath();
        ctx.restore();

    };


    return {
        ball: ball,
        square: square
    }
};

var EntityFactory = function (ctx, canvas) {
    var ball = function (radius) {
        var ball = new Entity(ctx);
        ball.position.x = canvas.width / 2;
        ball.position.y = canvas.height / 2;
        ball.dimensions.width = ball.dimensions.height = radius;
        ball.draw = new DrawFactory(ctx).ball;
        ball.position.bottom = ball.position.y;
        ball.position.top = ball.position.y - (ball.dimensions.height * 2);
        ball.position.left = ball.position.x;
        ball.position.right = ball.position.x + (ball.dimensions.width * 2);
        return ball;
    };

    var square = function (radius) {
        var square = new Entity(ctx);
        square.position.x = canvas.width / 2;
        square.position.y = canvas.height / 2;
        square.dimensions.width = square.dimensions.height = radius;
        square.draw = new DrawFactory(ctx).square;
        square.position.bottom = square.position.y;
        square.position.top = square.position.y - (square.dimensions.height * 2);
        square.position.left = square.position.x;
        square.position.right = square.position.x + (square.dimensions.width * 2);
        return square;
    };

    var bunny = function (size) {
        var bunny = new Entity(ctx);
        bunny.dimensions.width = bunny.dimensions.height = size;


    };

    var snake = function (segments, width) {
        segments = segments || 10;
        width = width || 15;
        var ent = new Entity(ctx);
        var snek = [];

        var percentageReduction = (1 / segments);
        var currentSize = 1;
        for (var i = 0; i < segments; i++) {
            var b = this.makeBall(Math.max(width * currentSize - (currentSize * percentageReduction), 1));
            currentSize -= percentageReduction;
            b.position.x = canvas.width / 2 + Math.random() * 5;
            b.position.y = canvas.height / 2 + Math.random() * 5;
            b.fillStyle = randomColor();
            b.acceleration = new Vector2d(0, 0);
            b.index = i;

            if (i == 0) {
                b.updatePreCall = function () {
                    var v = new Vector2d.subtractVectors(mousePos, this.position.center);
                    v.normalize();
                    v.multiply(5);

                    this.acceleration = v;
                };
            } else {
                b.updatePreCall = function () {
                    var v = new Vector2d.subtractVectors(ent.subEntities[this.index - 1].position.center, this.position.center);
                    v.normalize();
                    v.multiply(10);
                    this.acceleration = v;
                };
            }
            snek.push(b);
        }

        ent.position.x = canvas.width / 2;
        ent.position.y = canvas.height / 2;
        ent.dimensions.width = snek[0].dimensions.width;
        ent.dimensions.height = snek[0].dimensions.height;
        ent.subEntities = snek;

        ent.draw = function () {
            for (var a in this.subEntities) {
                this.subEntities[a].draw()
            }
        };
        ent.update = function () {
            for (var a in this.subEntities) {
                this.subEntities[a].update()
            }
        };
        return ent;
    };


    var squek = function (segments, width) {
        segments = segments || 10;
        width = width || 15;
        var ent = new Entity(ctx);
        var snek = [];

        var percentageReduction = (1 / segments);
        var currentSize = 1;
        for (var i = 0; i < segments; i++) {
            var b = this.makeSquare(Math.max(width * currentSize - (currentSize * percentageReduction), 1));
            currentSize -= percentageReduction;
            b.position.x = canvas.width / 2 + Math.random() * 5;
            b.position.y = canvas.height / 2 + Math.random() * 5;
            b.fillStyle = randomColor();
            b.acceleration = new Vector2d(0, 0);
            b.index = i;

            if (i == 0) {
                b.updatePreCall = function () {
                    var v = new Vector2d.subtractVectors(mousePos, this.position.center);
                    v.normalize();
                    v.multiply(5);
                    this.acceleration = v;

                };
            } else {
                b.updatePreCall = function () {
                    var v = new Vector2d.subtractVectors(ent.subEntities[this.index - 1].position.center, this.position.center);
                    v.normalize();
                    v.multiply(10);
                    this.acceleration = v;
                };
            }
            snek.push(b);
        }

        ent.position.x = canvas.width / 2;
        ent.position.y = canvas.height / 2;
        ent.dimensions.width = snek[0].dimensions.width;
        ent.dimensions.height = snek[0].dimensions.height;
        ent.subEntities = snek;

        ent.draw = function () {
            for (var a in this.subEntities) {
                this.subEntities[a].draw()
            }
        };
        ent.update = function () {
            this.internalTimer++;
            for (var a in this.subEntities) {
                this.subEntities[a].update()
            }
        };
        return ent;
    };


    return {
        makeBall: ball,
        makeSnake: snake,
        makeSquek: squek,
        makeSquare: square
    }
};

function sinWave(num){
    return Math.sin(num * (Math.PI/180));
}